﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SampleTestWebApp.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h2>.Net Test (Mohamad Hattarullah)</h2>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <label>Task 1 (Basic & Advance)</label>
                        </h4>
                    </div>
                    <div id="collapse1">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-5">Loan Amount (RM):</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputLoanAmount" value="500000" required="required" onkeypress="return isNumberKey(this, event);" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Loan Percentage (%):</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputLoanPercentage" required="required" value="90" onkeypress="return isNumberKey(this, event);" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Loan Interest (%):</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputLoanInterest" required="required" value="4.65" onkeypress="return isNumberKey(this, event);" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Loan Year:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="number" class="form-control" id="inputLoanYear" required="required" value="30" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Loan Monthly Repayment (RM):</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputLoanMonthly" value="2000" required="required" onkeypress="return isNumberKey(this, event);" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-5">
                                    <button runat="server" id="btnSubmit1" type="submit" class="btn btn-default" onserverclick="btnSubmit1_ServerClick">Get Monthly Amount</button>
                                    <button runat="server" id="btnSubmit2" type="submit" class="btn btn-default" onserverclick="btnSubmit2_ServerClick">Get Year</button>
                                    <button runat="server" id="btnSubmit3" type="submit" class="btn btn-default" onserverclick="btnSubmit3_ServerClick">Get Loan Amount</button>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 30px">
                            <div class="form-group">
                                <label>Results</label>
                            </div>
                            <div class="form-group">
                                <label>Task 1B Monthly Amount:</label>
                                <span runat="server" id="result1"></span>
                            </div>
                            <div class="form-group">
                                <label>Task 1 A1 Year:</label>
                                <span runat="server" id="result2"></span>
                            </div>
                            <div class="form-group">
                                <label>Task 1 A2 Loan Amount:</label>
                                <span runat="server" id="result3"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <label>Task 2 (Basic & Advance)</label>
                        </h4>
                    </div>
                    <div id="collapse2">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-5">Input Value:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="number" class="form-control" id="inputCheckDigit" value="726358263582647" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Min Value:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="number" class="form-control" id="inputMin" value="101" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Max Value:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="number" class="form-control" id="inputMax" value="909" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-5">
                                    <button runat="server" id="btnSubmit4" type="submit" class="btn btn-default" onserverclick="btnSubmit4_ServerClick">Get Check Digit</button>
                                    <button runat="server" id="btnSubmit7" type="submit" class="btn btn-default" onserverclick="btnSubmit7_ServerClick">Get Check Digit Distribution</button>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 30px">
                            <div class="form-group">
                                <label>Results</label>
                            </div>
                            <div class="form-group">
                                <label>Task 2B Check digit / Reference number:</label>
                                <span runat="server" id="result4"></span>
                            </div>
                            <div class="form-group">
                                <label>Task 2A Check digit distribution:</label>
                                <span runat="server" id="result7"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <label>Task 3 (Basic & Advance)</label>
                        </h4>
                    </div>
                    <div id="collapse3">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-5">Input Value:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputSortValue" value="1JD7H47FH2KD9G4" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Revert Value:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="text" class="form-control" id="inputRevertValue" value="oragmmvro!bin hktnas  dtaoemd  t typ sI ece.  ueoa  l lts sfmeatdati oiaaaustgeeses tno hfrhaCgecne tatnrlauey oeos" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Order:</label>
                                <div class="col-md-5">
                                    <input runat="server" type="number" class="form-control" id="inputOrder" value="18" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-5">
                                    <button runat="server" id="btnSubmit5" type="submit" class="btn btn-default" onserverclick="btnSubmit5_ServerClick">Get Sort</button>
                                    <button runat="server" id="btnSubmit6" type="submit" class="btn btn-default" onserverclick="btnSubmit6_ServerClick">Get Revert Sort</button>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 30px">
                            <div class="form-group">
                                <label>Results</label>
                            </div>
                            <div class="form-group">
                                <label>Task 3B Sort:</label>
                                <span runat="server" id="result5"></span>
                            </div>
                            <div class="form-group">
                                <label>Task 3A Revert Sort:</label>
                                <span runat="server" id="result6"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        function isNumberKey(txt, evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                //Check if the text already contains the . character
                if (txt.value.indexOf('.') === -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (charCode > 31 &&
                  (charCode < 48 || charCode > 57))
                    return false;
            }
            return true;
        }
    </script>
</body>
</html>
