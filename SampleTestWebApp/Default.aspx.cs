﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SampleTestWebApp
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string strLoanPercent = inputLoanPercentage.Value;
                string strLoanAmount = inputLoanAmount.Value;
                string strLoanInterest = inputLoanInterest.Value;
                string strLoanYear = inputLoanYear.Value;
                string strLoanMonthly = inputLoanMonthly.Value;
                string strCheckValue = inputCheckDigit.Value;
                string strSortValue = inputSortValue.Value;
                string strOrder = inputOrder.Value;
                string strRevertValue = inputRevertValue.Value;
                string strMin = inputMin.Value;
                string strMax = inputMax.Value;

                result1.InnerText = "RM " + Convert.ToString(Math.Round(CalculateMonthly(strLoanAmount, strLoanPercent, strLoanInterest, strLoanYear), 2));
                result2.InnerText = Convert.ToString(CalculateYear(strLoanAmount, strLoanPercent, strLoanInterest, strLoanMonthly));
                result3.InnerText = "RM " + Convert.ToString(Math.Round(CalculateMaxLoan(strLoanMonthly, strLoanPercent, strLoanInterest, "35"), 2));

                var digit = CheckDigit(strCheckValue);
                result4.InnerText = digit + " / " + strCheckValue + digit;
                var digit2 = checkDigitDistribution(Convert.ToInt32(strMin), Convert.ToInt32(strMax));
                result7.InnerHtml = "No, check digit can only be single digit, so it will be distributed between 1 to 9.<br />" + digit2;

                char[] input = strSortValue.ToCharArray();
                char[] output = cyclicSort(input, Convert.ToInt32(strOrder));
                result5.InnerText = new String(output);

                char[] input2 = strRevertValue.ToCharArray(); 
                char[] output2 = revertCyclicSort(input2, Convert.ToInt32(13));
                result6.InnerText = new String(output2);
            }
        }

        protected void btnSubmit1_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strLoanPercent = inputLoanPercentage.Value;
                string strLoanAmount = inputLoanAmount.Value;
                string strLoanInterest = inputLoanInterest.Value;
                string strLoanYear = inputLoanYear.Value;

                if (strLoanAmount.Equals(""))
                {
                    Message("Loan Amount (RM) is required.");
                    return;
                }
                else if (strLoanPercent.Equals(""))
                {
                    Message("Loan Percentage (%) is required.");
                    return;
                }
                else if (strLoanInterest.Equals(""))
                {
                    Message("Loan Interest (%) is required.");
                    return;
                }
                else if (strLoanYear.Equals(""))
                {
                    Message("Loan Year is required.");
                    return;
                }
                else
                {
                    result1.InnerText = "RM " + Convert.ToString(Math.Round(CalculateMonthly(strLoanAmount, strLoanPercent, strLoanInterest, strLoanYear), 2));
                }
            }
        }

        protected void btnSubmit2_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strLoanPercent = inputLoanPercentage.Value;
                string strLoanAmount = inputLoanAmount.Value;
                string strLoanInterest = inputLoanInterest.Value;
                string strLoanMonthly = inputLoanMonthly.Value;

                if (strLoanAmount.Equals(""))
                {
                    Message("Loan Amount (RM) is required.");
                    return;
                }
                else if (strLoanPercent.Equals(""))
                {
                    Message("Loan Percentage (%) is required.");
                    return;
                }
                else if (strLoanInterest.Equals(""))
                {
                    Message("Loan Interest (%) is required.");
                    return;
                }
                else if (strLoanMonthly.Equals(""))
                {
                    Message("Loan Monthly (RM) is required.");
                    return;
                }
                else
                {
                    result2.InnerText = Convert.ToString(CalculateYear(strLoanAmount, strLoanPercent, strLoanInterest, strLoanMonthly));
                }
            }
        }

        protected void btnSubmit3_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strLoanPercent = inputLoanPercentage.Value;
                string strLoanInterest = inputLoanInterest.Value;
                string strLoanMonthly = inputLoanMonthly.Value;
                string strLoanYear = inputLoanYear.Value;

                if (strLoanYear.Equals("") || Convert.ToInt32(strLoanYear) < 35)
                {
                    Message("Loan Year is required and more than 35 years.");
                    return;
                }
                else if (strLoanPercent.Equals(""))
                {
                    Message("Loan Percentage (%) is required.");
                    return;
                }
                else if (strLoanInterest.Equals(""))
                {
                    Message("Loan Interest (%) is required.");
                    return;
                }
                else if (strLoanMonthly.Equals(""))
                {
                    Message("Loan Monthly (RM) is required.");
                    return;
                }
                else
                {
                    result3.InnerText = "RM " + Convert.ToString(Math.Round(CalculateMaxLoan(strLoanMonthly, strLoanPercent, strLoanInterest, strLoanYear), 2));
                }
            }
        }

        protected void btnSubmit4_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strValue = inputCheckDigit.Value;

                if (strValue.Equals(""))
                {
                    Message("Input Value is required.");
                    return;
                }
                else
                {
                    var digit = CheckDigit(strValue);
                    result4.InnerText = digit + " / " + strValue + digit;
                }
            }
        }

        protected void btnSubmit5_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strValue = inputSortValue.Value;
                string strOrder = inputOrder.Value;

                if (strValue.Equals(""))
                {
                    Message("Input Value is required.");
                    return;
                }
                else if (strOrder.Equals(""))
                {
                    Message("Order is required.");
                    return;
                }
                else
                {
                    char[] input = strValue.ToCharArray();
                    char[] output = cyclicSort(input, Convert.ToInt32(strOrder));
                    result5.InnerText = new String(output);
                }
            }
        }

        protected void btnSubmit6_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strValue = inputRevertValue.Value;
                string strOrder = inputOrder.Value;

                if (strValue.Equals(""))
                {
                    Message("Revert Value is required.");
                    return;
                }
                else if (strOrder.Equals(""))
                {
                    Message("Order is required.");
                    return;
                }
                else
                {

                    char[] input2 = strValue.ToCharArray();
                    char[] output2 = revertCyclicSort(input2, Convert.ToInt32(strOrder));
                    result6.InnerText = new String(output2);
                }
            }
        }

        protected void btnSubmit7_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string strMin = inputMin.Value;
                string strMax = inputMax.Value;

                if (strMin.Equals("")) {
                    Message("Min Value is required.");
                    return;
                }
                else if (strMax.Equals(""))
                {
                    Message("Max Value is required.");
                    return;
                }
                else
                {
                    var digit = checkDigitDistribution(Convert.ToInt32(strMin), Convert.ToInt32(strMax));
                    result7.InnerHtml = "No, check digit can only be single digit, so it will be distributed between 1 to 9.<br />" + digit;
                }
            }
        }

        public static double CalculateMonthly(string strLoanAmount, string strLoanPercent, string strLoanInterest, string strLoanYear)
        {

            double LoanPercent = Convert.ToDouble(strLoanPercent);
            double LoanPercentRate = LoanPercent / 100;
            double LoanAmount = Convert.ToDouble(strLoanAmount);
            double LoanInterest = Convert.ToDouble(strLoanInterest);
            double LoanInterestRate = LoanInterest / 100;
            int LoanYear = Convert.ToInt32(strLoanYear);
            return (LoanPercentRate * LoanAmount) * ((LoanInterestRate / 12) * Math.Pow((1 + (LoanInterestRate / 12)), LoanYear * 12)) / (Math.Pow((1 + (LoanInterestRate / 12)), LoanYear * 12) - 1);
        }

        public static int CalculateYear(string strLoanAmount, string strLoanPercent, string strLoanInterest, string strLoanMonthly)
        {
            double LoanPercent = Convert.ToDouble(strLoanPercent);
            double LoanPercentRate = LoanPercent / 100;
            double LoanAmount = Convert.ToDouble(strLoanAmount);
            double LoanInterest = Convert.ToDouble(strLoanInterest);
            double LoanInterestRate = LoanInterest / 100;
            double LoanInterestRateYear = LoanInterestRate / 12;
            double LoanMonthly = Convert.ToInt32(strLoanMonthly);

            double total = Math.Log((LoanMonthly / ((LoanInterestRateYear * (LoanAmount * LoanPercentRate) - LoanMonthly))) * -1) / (12 * Math.Log(1 + LoanInterestRateYear));

            return Convert.ToInt32(total);
        }

        public static double CalculateMaxLoan(string strLoanMonthly, string strLoanPercent, string strLoanInterest, string strLoanYear)
        {

            double LoanPercent = Convert.ToDouble(strLoanPercent);
            double LoanPercentRate = LoanPercent / 100;
            double LoanMonthly = Convert.ToInt32(strLoanMonthly);
            double LoanInterest = Convert.ToDouble(strLoanInterest);
            double LoanInterestRate = LoanInterest / 100;
            int LoanYear = Convert.ToInt32(strLoanYear);
            double a = (LoanInterestRate / 12) * Math.Pow((1 + (LoanInterestRate / 12)), LoanYear * 12);
            double b = (Math.Pow((1 + (LoanInterestRate / 12)), LoanYear * 12) - 1);
            double c = LoanMonthly * b;
            double d = c / a;
            return (d / LoanPercentRate);
        }

        private void Message(string text)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + text + "')", true);
        }

        public static string CheckDigit(string inputValue)
        {
            int index = 0;
            int A = 0, B = 0, C = 0, D = 0;
            int finalSum;

            foreach (char c in inputValue.ToString())
            {
                int digit = Int32.Parse(c.ToString());

                if (index % 4 == 0)
                {
                    A = A + (digit * 8);
                }
                else if (index % 4 == 1)
                {
                    B = B + (digit * 6);
                }
                else if (index % 4 == 2)
                {
                    C = C + (digit * 4);
                }
                else if (index % 4 == 3)
                {
                    D = D + (digit * 2);
                }

                index++;
            }

            finalSum = A + B + C + D;

            while (finalSum.ToString().Length > 1)
            {
                int oldFinalSum = finalSum;
                finalSum = 0;

                foreach (char c in oldFinalSum.ToString())
                {
                    finalSum = finalSum + Int32.Parse(c.ToString());
                }
            }

            return finalSum.ToString();
        }

        public static string checkDigitDistribution(int min, int max)
        {
            int i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0, i6 = 0, i7 = 0, i8 = 0, i9 = 0;
            string countSummary = "";

            for (int i = min; i <= max; i++)
            {
                int checkDigit = Int32.Parse(CheckDigit(i.ToString()));
                switch (checkDigit)
                {
                    case 1:
                        i1++;
                        break;
                    case 2:
                        i2++;
                        break;
                    case 3:
                        i3++;
                        break;
                    case 4:
                        i4++;
                        break;
                    case 5:
                        i5++;
                        break;
                    case 6:
                        i6++;
                        break;
                    case 7:
                        i7++;
                        break;
                    case 8:
                        i8++;
                        break;
                    case 9:
                        i9++;
                        break;
                }
            }

            countSummary = countSummary + "Digit 1 has " + i1.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 2 has " + i2.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 3 has " + i3.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 4 has " + i4.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 5 has " + i5.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 6 has " + i6.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 7 has " + i7.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 8 has " + i8.ToString() + " count." + "<br />";
            countSummary = countSummary + "Digit 9 has " + i9.ToString() + " count." + "<br />";
            return countSummary;
        }


        public static char[] cyclicSort(char[] input, int ordering)
        {
            List<char> inputList = input.ToList();
            List<char> outputList = new List<char>();
            int maxLength = input.Length;
            int lastIndex = 0;
            int actualIndex;

            while (inputList.Count > 0)
            {
                actualIndex = (lastIndex + ordering) % inputList.Count() - 1;
                if (actualIndex < 0)
                {
                    actualIndex = inputList.Count() - 1;
                }
                lastIndex = actualIndex;
                outputList.Add(inputList[actualIndex]);
                inputList.RemoveAt(actualIndex);
            }

            return outputList.ToArray();
        }

        public static char[] revertCyclicSort(char[] output, int ordering)
        {
            List<char> outputList = output.ToList();
            List<char> outputListRunning = new List<char>();
            outputListRunning.AddRange(outputList);
            int outputListCount = outputList.Count();
            List<char> inputList = new List<char>(new Char[outputListCount]);
            int index = 0;
            int count = 0;
            List<int> indexArray = new List<int>();

            for (int i = 0; i < outputList.Count; i++)
            {
                Char c = outputList[i];

                while (count < ordering)
                {
                    index++;

                    if (index >= outputListCount)
                    {
                        index = 0;
                    }

                    if (outputListRunning[index] != '\0')
                    {
                        count++;
                    }
                }

                inputList[index] = c;
                outputListRunning[index] = '\0';

                count = 0;
            }

            inputList.Add(inputList[0]);
            inputList.Remove(inputList[0]);
            return inputList.ToArray();
        }

    }
}